<?php

namespace App\Repository;

use App\Entity\Feed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Feed|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feed|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feed[]    findAll()
 * @method Feed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Feed::class);
    }

    public function findByDate($date){
        return $this->createQueryBuilder('f')
            ->andWhere('DATE(f.date) = :date')
            ->setParameter('date', $date, \Doctrine\DBAL\Types\DateType::DATE)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByProviderAndDate($publisher, $date){
        return $this->createQueryBuilder('f')
            ->andWhere('DATE(f.date) = :date')
            ->andWhere('f.publisher = :publisher')
            ->setParameter('date', $date, \Doctrine\DBAL\Types\DateType::DATE)
            ->setParameter('publisher', $publisher)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Feed[] Returns an array of Feed objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Feed
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
