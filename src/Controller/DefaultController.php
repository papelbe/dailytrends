<?php

namespace App\Controller;

use App\Entity\Feed;
use App\Repository\FeedRepository;
use App\Service\Feeder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(FeedRepository $feedRepository, Feeder $feeder): Response
    {
        $feeder->getFeeds();

        $today = new \DateTime();
        $em = $this->getDoctrine()->getManager();
        $feeds = $em->getRepository(Feed::class)->findByDate($today);

        return $this->render('default/index.html.twig', [
            'feeds' => $feeds,
        ]);
    }
}
