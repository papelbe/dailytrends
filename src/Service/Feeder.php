<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Goutte\Client;
use App\Entity\Feed;
use Symfony\Component\DomCrawler\Crawler;

class Feeder {

    const PROVIDERS = [
        'Pais' => [
            'name' => 'El Pais',
            'url' => 'http://elpais.com',
            'tag_article' => 'article',
            'link_article' => 'h2 > a',
            'body_paragraphs' => '.articulo-cuerpo > p',
            'title' => 'meta[property=\'og:title\']',
            'image' => 'meta[property=\'og:image\']',
            'source' => 'meta[name=author]',
            'publisher' => 'meta[name=organization]'
        ],
        'Mundo' => [
            'name' => 'El Mundo',
            'url' => 'https://www.elmundo.es',
            'tag_article' => 'article',
            'link_article' => 'a > h2',
            'body_paragraphs' => 'div[data-section="articleBody"] > p',
            'title' => 'meta[property=\'og:title\']',
            'image' => 'meta[data-ue-u=\'og:image\']',
            'source' => 'div.ue-c-article__byline-name',
            'publisher' => 'meta[name=organization]'
            ]
        ];

    private $em;
    private $feeds = [];

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /**
     * Get feeds from providers defined as constant if not exist in db yet
     * @throws \Exception
     */
    public function getFeeds() {

        foreach (self::PROVIDERS as $provider){
            if(!$this->todayFeedsFromProvider($provider)){
                $this->getFeedsFromProvider($provider);
            }
        }
    }

    /**
     * Get first feed from provider given
     * @param $provider
     */
    public function getFeedsFromProvider($provider){

        $client = new Client();
        $crawler = $client->request('GET', $provider['url']);

        $crawler->filter($provider['tag_article'])->slice(0, 1)->each(function($article, $i) use ($client, $provider) {
            try{
                $articleUrl = $article->selectLink($article->filter($provider['link_article'])->text())->link()->getUri();
                $articlePage = $client->request('GET', $articleUrl);
                $this->createExternalFeed($articlePage, $provider);
            }catch(\InvalidArgumentException $e){}

        });
    }

    /**
     * Create a feed from an article
     * @param Crawler $article
     * @param $provider
     * @throws \Exception
     */
    public function createExternalFeed(Crawler $article, $provider){

        $title = $article->filter($provider['title'])?
            $article->filter($provider['title'])->attr('content') : '-';

        $image = $article->filter($provider['image'])?
            $article->filter($provider['image'])->attr('content') : '-';

        if($provider['name'] == 'El Pais'){
            $source = $article->filter($provider['source'])?
                $article->filter($provider['source'])->attr('content') : '-';
        }else{
            $source = $article->filter($provider['source'])?
                $article->filter($provider['source'])->text() : '-';
        }

        $body = '-';
        $body = $article->filter($provider['body_paragraphs'])->each(function($paragraph, $i){
            return '<p>'.$paragraph->html().'</p>';
        });

        $feed = new Feed();
        $feed->setTitle($title);
        $feed->setImage($image);
        $feed->setSource($source);
        $feed->setPublisher($provider['name']);
        $feed->setBody(implode($body));
        $feed->setDate(new \DateTime());

        $this->em->persist($feed);
        $this->em->flush();

        $this->feeds[] = $feed;
    }

    /**
     * Check if there are today's feeds on DB from a given provider
     * @param $provider
     * @return bool
     * @throws \Exception
     */
    public function todayFeedsFromProvider($provider): bool {
        $today = new \DateTime();
        $feeds = $this->em->getRepository(Feed::class)->findByProviderAndDate($provider['name'], $today);

        if(count($feeds) > 0){
            return true;
        }

        return false;
    }
}